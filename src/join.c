#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include "utils.h"
#define FALSE 0
#define TRUE 1

struct option long_options[] =
        {
            {"join", 1, 0, 'j'},
            {"part", 1, 0, 'p'},
        };

FILE *file;
int j;

void join(char *nomArchivo, int n, char* faltante){

    pid_t pid=0;
    int i=0;
    int ide=0;
    int tam_parte=2;
    int fd[2];
    int fd1[2];
    int fd2[2];
    int fd3[2];
    int aumento=0;
    int status;

    pipe(fd);
    pipe(fd1);
    pipe(fd2);
    pipe(fd3);

    // si no estan todos los archivos y solamente falta uno
    if( strcmp(faltante, "todos") != 0 ){
        printf("falta un archivo, se regenerara\n");
        pid=1;
        i=0;
    }
    else{
        printf("estan todos los archivos\n");
        i=n;
        pid=1;
    }


    //mientras sea el padre, creará n+1 hijos y si es hijo saldrá sin realizar fork
    while(pid!=0 && i!=n+1 ){

        pid=fork(); // creamos un nuevo proceso hijo

        // si hay un error al crear un hijo, arrojar mensaje
        if(pid==-1){

            perror("Hubo un error y no se pudo crear el proceso hijo");
            exit(-1);
        }

        // si es el proceso padre
        if(pid!=0){

            j=i;
            write(fd[1],&j,sizeof(int)); // envia el ide a cada hijo a travez de un pipe
            waitpid(-1, &status, 0);
        }

            i++; // aumentamos i para la siguiente iteracion del while
    }

    //todos los hijos empiezan del principio a leer
    //si es hijo
    if(pid==0){

        read(fd[0],&ide,sizeof(int)); // le asignamos a cada hijo un IDE

        //si es el hijo con ide=0 lee una parte del archivo solo envia su informacion no calcula xor
        if(ide==0){

            char c;

            char *nombreBase = concatenar(nomArchivo, ".part");
            char *num = numToChar(ide+1);
            char *nombre = concatenar(nombreBase, num);
            int cont=0;
            free(num);


            if(strcmp(nombre,faltante)==0){

                aumento++;
                char *num = numToChar(ide+1+aumento);
                nombre = concatenar(nombreBase, num);
                free(num);
            }

            write(fd3[1],&aumento,sizeof(int));

            file=fopen(nombre,"r");

            while( fread(&c ,sizeof(char), 1, file) != 0){
                write(fd1[1], &c, sizeof(char)); // lo leido lo enviamos a otro proceso


                cont ++;
            }

            fclose(file);

        }


        // si es uno de los hijos con ide mayor a 0 y menor a n-1, lee una de las p-2 partes de archivos restantes, genera xor de lo leido y envia
        if(ide<n-1 && ide>0){

            // si el ide del proceso hijo es par
             if((ide%2)==0 ){

                    char c1=0;
                    char c2=0;
                    int xor1=0;
                    char *nombre;
                    char *nombreBase = concatenar(nomArchivo, ".part");

                    read(fd3[0],&aumento,sizeof(int));

                    if(aumento==0){ //aun no se encuentra el archivo faltante entre los nombres generados

                        char *num = numToChar(ide+1);
                        nombre = concatenar(nombreBase, num);
                        free(num);

                        // comprobar si el nombre formado es el faltante
                        if(strcmp(nombre,faltante)==0){ // si es igual cambiamos el nombre

                            aumento++;
                            char *num = numToChar(ide+1+aumento);
                            nombre = concatenar(nombreBase, num);
                            free(num);
                        }
                    }

                    else{

                        char *num = numToChar(ide+1+aumento);
                        nombre = concatenar(nombreBase, num);
                        free(num);

                    }
                    write(fd3[1],&aumento,sizeof(int));

                    file = fopen(nombre,"r"); 

                    while(fread(&c1, sizeof(char), 1, file) != 0){

                        read(fd2[0],&c2,sizeof(char)); // leemos desde el descriptor fd2

                        xor1 = c1 ^ c2 ; //calculo de xor

                        write(fd1[1], &xor1, sizeof(char)); //enviamos el xor a otro proceso
                    }

                    fclose(file);

                }

                // si el ide del proceso hijo es impar
                else{

                    char c1 = 0;
                    char c2 = 0;
                    int xor1 = 0;
                    char *nombre = NULL;
                    char *nombreBase = concatenar(nomArchivo, ".part");

                    // obtiene el aumento que se acarreaa del otro proceso
                    read(fd3[0],&aumento,sizeof(int));

                    // hay que comprobar si esta el archivo
                    if(aumento==0){

                        char *num = numToChar(ide+1);
                        nombre = concatenar(nombreBase, num);
                        free(num);

                        // si es el archivo faltante
                        if(strcmp(nombre,faltante)==0){

                            aumento++;
                            // lee el siguiente archivo
                            char *num = numToChar(ide+1+aumento);
                            nombre = concatenar(nombreBase, num);
                            free(num);
                        }
                    }
                    else{

                        char *num = numToChar(ide+1+aumento);
                        nombre = concatenar(nombreBase, num);
                        free(num);

                    }
                    write(fd3[1],&aumento,sizeof(int));

                    file = fopen (nombre,"r"); 

                    int cont=0;
                    while(fread(&c1, sizeof(char), 1, file) != 0){
                        cont++;
                        read(fd1[0],&c2,sizeof(char)); // leemos desde el descriptor fd1

                        xor1 = c1 ^ c2 ; //calculamos xor


                        write(fd2[1], &xor1, sizeof(char)); // enviamos el xor a los demas procesos
                    }

                    fclose(file);


                }

        }

        // si es el hijo con ide n-1 este leera el archivo xor, recibe de los otros hijos la informacion y reconstruira el archivo faltante
        if(ide==n-1){

             // si el ide del proceso hijo es par
             if((ide%2)==0){

                    char c1=0;
                    char c2=0;
                    int xor1=0;
                    char *nombre= concatenar(nomArchivo, ".xor");

                    file = fopen(nombre, "r"); //abre el archivo .xor
                    FILE *recuperado = fopen(faltante,"w"); // creamos un archivo para escribir en el la parte que recuperaremos

                    while(fread(&c1, sizeof(char), 1, file) != 0){

                        read(fd2[0],&c2,sizeof(char)); // leemos desde el descriptor fd2
                        xor1 = c1 ^ c2 ;

                        fwrite(&xor1, sizeof(char), 1, recuperado); //escribimos en el archivo recuperado

                    }

                    fclose(recuperado);
                    fclose(file);
            }

            // si el ide del proceso hijo es impar
            else{

                char c1=0;
                char c2=0;
                int xor1=0;
                char *nombre= concatenar(nomArchivo, ".xor");

                file = fopen(nombre, "r"); // abre el archivo xor
                FILE *recuperado = fopen(faltante,"w"); //creamos un archivo para escribir en el

                while(fread(&c1, sizeof(char), 1, file) != 0){

                    read(fd1[0],&c2,sizeof(char)); // leemos desde el descriptor fd1
                    xor1 = c1 ^ c2 ;

                    fwrite(&xor1, sizeof(char), 1, recuperado); //escribirmos el xor en el archivo recuperado

                }

                fclose(recuperado);
                fclose(file);
            }
        }
    }

    // si es el hijo con ide=n este proceso es uno aparte que solo realizará la union de los archivos
    if(pid==0 && ide==n){
        unir(nomArchivo,tam_parte,n);
    }
}


int main(int argc, char **argv){
    int partes = 0;
    char *nombre_archivo = NULL;
    char c = 1;
    char *faltante = NULL;

    while(c){
        int option_index = 0;

        c = getopt_long (argc, argv, "jp:", long_options, &option_index);

        switch(c){
            case 'j':
                nombre_archivo = optarg;
                break;
            case 'p':
                partes = atoi(optarg);
                break;
            case '?':
                break;
            default:
                c = 0;
        }
    } 

    // comprueba si falta una parte o si falta mas de una o si estan todas
    // si retorna NULL, hay mas de dos partes faltantes
    // retorna "todos" si estan todas las partes
    // si retorna algo diferente de NULL o "todos" => falta una parte
    faltante=parteFaltante(nombre_archivo,partes);

    if( faltante == NULL){
        exit(-1);
    }

    join(nombre_archivo, partes, faltante);

    return 0;
}
