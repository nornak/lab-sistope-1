#ifndef _UTILS
#define _UTILS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 20

char *concatenar(char *str1, char *str2);
char *numToChar(unsigned int num);
char *parteFaltante( char *archivo, int numPart);
void unir(char* nomArchivo,int tam_parte, int n );

#endif
