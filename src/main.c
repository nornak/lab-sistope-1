#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>

#define FALSE 0
#define TRUE 1

/*
 * estructura por defecto que recibe la funcion getopt_long
 * el primer strin es nombre de la opcion larga
 *  el 2º indica si necesita un argumento, 0 no necesita y 1 si necesita
 *  el 3º indica como se devuelve los valores, si es nula 
 *  la funcion devuelve el 4º valor
 *  4º valor, valor que se devuelve o la que se carga en la 3º variable
 */
struct option long_options[] =
         {
           {"cut",  1, 0, 'c'},
           {"join", 1, 0, 'j'},
           {"part", 1, 0, 'p'},
           {0, 0, 0, 0}
         };

int main(int argc, char **argv){
    // 0: false, 1: true
    int opc_cut = FALSE;
    int opc_parte = FALSE;
    int opc_join = FALSE;
    char *nombre_archivo = NULL;
    // almacena los valores de los argumentos
    char *valor_parte = NULL;
    char c = 1;


    while (c){
        int option_index = 0;
     
        c = getopt_long (argc, argv, "cjp:", long_options, &option_index);

        switch(c){
            case 'c':
                opc_cut = TRUE;
                nombre_archivo = optarg;
                break;
            case 'p':
                valor_parte = optarg;
                opc_parte = TRUE;
                break;
            case'j':
                opc_join = TRUE;
                nombre_archivo = optarg;
                break;
            case '?':    
                if( optopt == 'c' ){
                    fprintf (stderr, "Opcion --cut requiere un argumento.\n");
                }
                break;
            default:
                printf("cuak!\n");
                c=0;
        }
    }

    

    if(opc_cut && opc_parte){
        printf("cut: %s\n", valor_parte);
        printf("valor: %s\n", nombre_archivo);
        execl("bin/cortar", "cortar", "--cut", nombre_archivo, "-p", valor_parte, (char*)NULL);
    }
    if(opc_join && opc_parte){
        printf("join: %s\n", valor_parte);
        printf("valor: %s\n", nombre_archivo);
        execl("bin/join", "join", "--join", nombre_archivo, "-p", valor_parte, (char*)NULL);
    }
    
    return 1;
}
