#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "utils.h"

FILE *file;

int j;

static struct option long_options[] =
         {
           {"cut",  1, 0, 'c'},
           {"part", 1, 0, 'p'},
           {0, 0, 0, 0}
         };

// Funcion que crea n=P procesos, asociados al mismo padre, tambien comunica padres con hijos e hijos con hijos

void crearNProcesos(int n, int p, char *nombre_archivo, int falta){

        int pid; // un identificador para cada proceso
        int i; // para las iteraciones del while que creará los n hijos
        int ide; // para otorgar un IDE a cada hijo, que nos servira para organizar las particiones
        int fd[2]; // pipe para comincar al padre con sus hijos para poder enviar el ide
        int fd1[2]; // 2 pipe fd1 y fd2 para comunicacion entre procesos hijos
        int fd2[2];
        int tam_parte = p; //el tamaño que tendra la parte calculada en el main
        printf("tam_parte %d \n" , tam_parte);


        // Creamos los 3 pipes es decir 6 descriptores
        pipe(fd);
        pipe(fd1);
        pipe(fd2);

        pid=1; // le asignamos uno a pid para asegurarnos que entre al while
        i=0; // inicializamos i con 0


        //MIENTRAS SEA EL PADRE, CREARÁ N HIJOS Y SI ES HIJO SALDRÁ SIN REALIZAR FORK
        while(pid!=0 && i!=n ){

            pid=fork(); // creamos un nuevo proceso hijo

            // SI HAY UN ERROR AL CREAR UN HIJO, ARROJAR MENSAJE
            if(pid==-1){

                perror("Hubo un error y no se pudo crear el proceso hijo");
                exit(-1);

            }

            // SI ES EL PROCESO PADRE
            if(pid!=0){

                j=i;
                write(fd[1],&j,sizeof(int)); // envia el ide a cada hijo a travez de un pipe
                waitpid(-1, NULL, 0);

            }

            i++; // aumentamos i para la siguiente iteracion del while
        }



        // SI ES UN PROCESO HIJO
        if(pid==0){

            read(fd[0],&ide,sizeof(int)); // un hijo lee el ide enviado por el padre


            // COMUNICACION ENTRE "HERMANOS"

            // El ide otorgado a cada hijo se utilizara para destinar y determinar que hijo tomara la primera parte del archivo,
            // alguna parte intermedia y la ultima parte

            // Para la comunicacion entre procesos hijos se realizaron 2 pipe, fd1 y fd2
            // segun el ide de los hijo se destino la comunicacion de la siguiente forma:
            // 1. Para el proceso hijo que debe leer la primera parte del archivo (con ide = 0 considerada como par) solo escribe en fd1
            // 2. Para los procesos hijos que leen una parte intermedia hay 2 casos, si su ide es par o impar.
            //      2.1 si el ide es par leera de fd2 y escribira en fd1
            //      2.2 si el ide es impar leera de fd1 y escribira de fd2
            // 3. Para el proceso hijo que lee la ultima parte del archivo solo lee y debe verificar las condiciones del paso 2 ya que no se sabe si
            // su ide sera par o impar

            // 1. ES EL PROCESO HIJO QUE DEBE TOMAR LA PRIMERA PARTE DEL ARCHIVO
            if(ide==0){

                int cont=0;
                size_t c; // para leer un byte del archivo que el corresponda
                char *nombre = concatenar(nombre_archivo, ".part"); // concatenamos para generar el nombre de la parte
                char *num = numToChar(ide+1);  // el nombre de la parte sera el ide mas 1 (ide 0 toma parte 1)

                nombre = concatenar(nombre, num); // concatenamos para agregar el numero de la parte
                free(num); //liberamos memoria

                FILE *salida = fopen(nombre, "wb"); //abrimos un archivo para generar la parte ide+1

                //fread(&c, sizeof(char), 1, file); // leemos desde el principio por lo que no utilizamos seek
                fread(&c, 1, 1, file); 

                // mientras no leamos los bytes igual al tamaño de una parte
                while(cont != tam_parte){

                    //printf(" c:%i", c);
                    fwrite(&c, sizeof(char), 1 , salida); // escribimos 1 byte en la parte
                    //fwrite(&c, 1, 1 , salida); 
                    write(fd1[1], &c, 1); // enviamos el caracter leido al descriptor fd1
                    //fread(&c, sizeof(char), 1, file); // seguimos leyendo
                    fread(&c, 1, 1, file); // seguimos leyendo
                    cont++;
                }
                fclose(salida); //cerramos el archivo
                printf("cont1: %d\n", cont);


            }

            // 2. SON LOS PROCESOS QUE TOMAN UNA PARTE INTERMEDIA DEL ARCHIVO (SIN CONTAR LA PRIMERA PARTE NI LA ULTIMA)
            if(ide!=0 && ide!=n-1){


                // 2.1 SI EL IDE DEL PROCESO HIJO ES PAR
                if((ide%2)==0){

                    size_t c1=0; // para leer desde el archivo correspondiente
                    size_t c2=0; // para leer desde el descriptor correspondiente
                    int cont=0;
                    size_t xor1=0; // para guardar el resultado de la funcion xor entre c1 y c2
                    fseek(file, tam_parte*ide, SEEK_SET); // nos desplazamos en el archivo tam_parte*ide para leer desde donde corresponde

                    char *nombre = concatenar(nombre_archivo, ".part"); // concatenamos el nombre
                    char *num = numToChar(ide+1); // creamos el numero como string
                    nombre = concatenar(nombre, num); // creamos el nombre
                    free(num); // liberamos memoria

                    FILE *salida = fopen(nombre, "wb"); // abrimos un archivo para generar la parte

                    fread(&c1, 1, 1, file); // leemos un byte del orignal

                    // mientras no leamos los bytes igual al tamaño de una parte
                    while(cont!=tam_parte){

                        fwrite(&c1, 1, 1, salida); // escribimos un byte en la parte
                        read(fd2[0], &c2, 1); // leemos desde el descriptor fd2
                        xor1 = c1 ^ c2 ;  // realizamos xor de los datos leidos

                        write(fd1[1], &xor1, 1); // escribimos en el descriptor fd1
                        cont++;
                        fread(&c1, 1, 1, file); // seguimos leyendo del original
                    }

                    printf("cont2: %d\n", cont);
                    fclose(salida); // cerramos archivo
                }

                // 2.2 SI EL IDE DEL PROCESO HIJO ES IMPAR
                else{

                    size_t c1 = 0; // para leer desde el archivo correspondiente
                    size_t c2 = 0; // para leer desde el descriptor correspondiente
                    int cont = 0;
                    size_t xor1 = 0; // para guardar el resultado de la funcion xor entre c1 y c2
                    char *nombre = NULL;
                    char *num = NULL;

                    nombre = concatenar(nombre_archivo, ".part");  // concatenamos el nombre
                    num = numToChar(ide+1); // creamos el numero como string
                    nombre = concatenar(nombre, num); // concatenamos el numero al nombre
                    free(num);

                    fseek(file, tam_parte*ide, SEEK_SET); // realizamos seek para leer desde donde se debe
                    FILE *salida = fopen(nombre, "wb"); // abrimos un archivo para crear la parte

                    fread(&c1, 1, 1, file); // leemos desde el original

                    // mientras no leamos los bytes igual al tamaño de una parte
                    while(cont!=tam_parte){

                        fwrite(&c1, 1, 1, salida); // escribimos en al parte
                        read(fd1[0],&c2, 1); // leemos desde el descriptor fd1
                        xor1 = c1 ^ c2 ; // hacemos sor entre los datos leidos

                        write(fd2[1], &xor1, 1); // escribimos en el descritor fd2
                        cont++;
                        fread(&c1, 1, 1, file); // seguimos leyendo desde el original
                    }

                    printf("cont3: %d\n", cont);
                    fclose(salida);
                }
            }

            // 3. EL PROCESO QUE TOMA LA ULTIMA PARTE DEL ARCHIVO Y DEBE GENERAR EL ARCHIVO CON EL XOR
            if(ide==n-1){

                // 3.1 SI EL IDE DEL PROCESO HIJO ES PAR
                if((ide%2)==0){

                    size_t c1=0; // para leer desde el archivo correspondiente
                    size_t c2=0; // para leer desde el descriptor correspondiente
                    int cont=0;
                    size_t xor1=0; // para guardar el resultado de la funcion xor entre c1 y c2
                    char *nombre = NULL;
                    char *num = NULL;
                    int limite = tam_parte - falta; // pasa saber hasta donde leer y comenzar a agregar ceros
                    char *salida_xor = NULL;

                    fseek(file, tam_parte*ide, SEEK_SET); // comenzamos a leer desde donde debemos desde el original

                    nombre = concatenar(nombre_archivo, ".part"); // concatenamos el nombre
                    num = numToChar(ide+1); // transformamos el numero a estring
                    nombre = concatenar(nombre, num); // concatenamos nombre con el numero
                    free(num);

                    salida_xor = concatenar(nombre_archivo, ".xor"); // creamos el nombre del archivo xor

                    FILE *salida = fopen(nombre, "wb"); // abrimos el archivo para escribir la salida
                    FILE *f_xor= fopen(salida_xor, "wb"); // abrimos el archivo para escribir el xor

                    fread(&c1, 1, 1, file); // leemos desde el original

                    // mientras no leamos los bytes igual al tamaño de una parte
                    while(cont!=tam_parte){

                        if ( cont < limite ){ // debemos escribir un byte del archivo

                            fwrite(&c1, 1, 1 , salida); // escribimos desde el original
                            read(fd2[0], &c2, 1); // leemos desde el descriptor fd2
                            xor1 = c1 ^ c2 ; // realizamos xor

                            fwrite(&xor1, 1, 1 , f_xor); //escribimos en el archivo xor

                            cont++;
                            fread(&c1, 1, 1, file); // seguimos leyendo del original
                        }

                        else{ // si debemos agregar ceros

                            c1 = 0; // caracter que agrega ceros
                            fwrite(&c1, 1, 1 , salida); //escribimos en la parte
                            read(fd2[0], &c2, 1); // leemos desde el descriptor fd2

                            xor1 = c1 ^ c2 ; // se hacer el xor

                            fwrite(&xor1, 1, 1 , f_xor); // escribimos en el archivo xor


                            cont++;

                        }
                    }
                    printf("cont3: %d\n", cont);
                    fclose(salida);
                }

                // 3.2 SI EL IDE DEL PROCESO HIJO ES IMPAR
                else{

                    size_t c1 = 0; // para leer desde el archivo correspondiente
                    size_t c2 = 0; // para leer desde el descriptor correspondiente
                    int cont = 0;
                    size_t xor1 = 0; // para guardar el resultado de la funcion xor entre c1 y c2
                    char *nombre = NULL;
                    char *num = NULL;
                    char *salida_xor = NULL;
                    int limite = tam_parte - falta; // para saber si agregar o no ceros

                    fseek(file, tam_parte*ide, SEEK_SET); // para empezar a leer desde corresponde

                    nombre = concatenar(nombre_archivo, ".part"); // concatenamos nombre
                    num = numToChar(ide+1); // transformamos el numero a string
                    nombre = concatenar(nombre, num); // concatenamos nombre con el numero
                    free(num);

                    salida_xor = concatenar(nombre_archivo, ".xor"); // concatenamos el nombre para el archivo xor

                    FILE *salida = fopen(nombre, "wb"); // abrimos un archivo para escribir la parte
                    FILE *f_xor= fopen(salida_xor, "wb"); // abrimos el archivo para el xor

                    fread(&c1, 1, 1, file); // leemos del original

                    while(cont!=tam_parte){ // mientars leamos byte y no agreguemos ceros

                        if ( cont < limite ){

                            fwrite(&c1, 1, 1 , salida); // escribimos en la parte
                            read(fd1[0],&c2, 1); // leemos desde el descriptor fd1
                            xor1 = c1 ^ c2 ;

                            fwrite(&xor1, 1, 1 , f_xor); // escribimos en el xor

                            cont++;
                            fread(&c1, 1, 1, file); // seguimos leyendo del original
                        }
                        else{

                            c1 = 0; // para agregar los ceros
                            fwrite(&c1, 1, 1 , salida); // escribimos en la parte
                            read(fd1[0],&c2,1); // leemos desde el descriptor fd1
                            xor1 = c1 ^ c2 ; // realizamos xor

                            fwrite(&xor1, 1, 1 , f_xor); // escribimos en el xor los ceros

                            cont++;

                        }

                    }
                    printf("cont4: %d\n", cont);
                    fclose(salida);
                }
            }
        }// FIN DEL SI ES HIJO
}

// guarda la cantidad de ceros ingresada al archivo
void guardar_temporal(char *nombre, int falta){
    printf("falta %d \n" , falta);
    char *nombre_temp = concatenar(nombre, ".dato");
    FILE *temp = fopen(nombre_temp, "w");


    fwrite(&falta, sizeof(int), 1, temp);

    fclose(temp);
}



int main(int argc, char **argv){

    char *nombre_archivo = NULL;
    int num_parte = 0;
    int c = 1;
    int largo=0;
    int tam_parte=0;
    int falta = 0;

    while(c){
        int option_index = 0;

        c = getopt_long(argc, argv, "cp:", long_options, &option_index);
        switch(c){
            case 'c':
                nombre_archivo = optarg;
                break;
            case 'p':
                num_parte = atoi(optarg);
                break;
            case '?':
                break;
            default:
                c=0;
        }
    }
    if(nombre_archivo == NULL || num_parte == 0){
        exit(-1);
    }

    file = fopen(nombre_archivo, "rb");

    if(file == NULL ){
        printf("archivo no encontrado. \n");
        exit(-1);
    }

    // para calcular el largo del archivo
    fseek(file, 0, SEEK_END);
    largo = ftell(file);

    fseek(file, 0, SEEK_SET);

    tam_parte = largo/num_parte;

    printf("tamparte %d: \n", tam_parte);

    if(largo%num_parte != 0){
        tam_parte+=1;
    }

    falta = tam_parte*num_parte - largo;

    guardar_temporal(nombre_archivo, falta);


    crearNProcesos(num_parte, tam_parte, nombre_archivo, falta);

    fclose(file);

    return 1;
}


