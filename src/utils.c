#include "utils.h"
// crea un nuevo string concatenando str1 y str2
char *concatenar(char *str1, char *str2){

    int largo = strlen(str1) + strlen(str2) + 1;
    char *buf = (char*)malloc(sizeof(char)*largo);
    memset(buf, '\0', largo);
    strcat(buf, str1);
    strcat(buf, str2);

    return buf;
}

char *numToChar(unsigned int num){

    int digito;
    int i=0,j=0;
    char *numeroReves = (char*)malloc(MAX*sizeof(char)); //falta hacerlo para largo n
    memset(numeroReves, '\0', MAX);

    while(num > 0){ //mientras se pueda conseguir un digito mas

        digito = num%10; // encontramos el digito con el resto de la division por 10
        numeroReves[i]= digito+48; //convertimos el digito en caracter
        num/=10;
        i++; // se realizan i digitos y en el arreglo dinamico se ocupo hasta la casilla i-|
    }

    char *numero = (char*)malloc(i+1*sizeof(char));
    memset(numero, '\0', (i+1));

    while(i!=0){

        numero[j]=numeroReves[i-1];
        i--;
        j++;
    }

    free(numeroReves);

    return numero;
}


// Funcion que verifica si faltan o no partes de un archivo
// Si falta una parte retorna el nombre del archivo de la parte faltante
// Si faltan mas de una parte retorna NULL, y significa que no se podrá recuperar el archivo
// Si estan todas las partes retorna el string "todos"

char *parteFaltante( char *archivo, int numPart){


    int digitos=0;
    int p=numPart;

    while(p>0){

        p/=10;
        digitos++;
    }


    char *nombreBase;
    char *nombre;
    char *faltante;
    char *num;
    int i=0;
    int contador=0;


    nombreBase=concatenar(archivo,".part");


    while(i!=numPart){

            num=numToChar(i+1);
            nombre=concatenar(nombreBase,num);

            FILE *archivo;
            archivo=fopen(nombre,"r");

            if (archivo==NULL){ // si no se puede abrir el archivo para lectura

                faltante=nombre;
                //printf("%s\n",nombre);
                contador++; // contamos cuantos archivos faltan

            }

            else{

                fclose(archivo);

            }

            i++;

    }

    free(nombreBase);

    // Si falta solo una parte
    if(contador==1){

        return faltante;
    }

    else{

        // Si falta  mas de una parte
        if(contador>1){

            return NULL;
        }

        //si no falta ninguna parte
        else{

            return "todos";

        }

    }
}

//Funcion que uno n archivos de tamaño tam_parte, debe recibir el nombre del archivo original
void unir(char* nomArchivo,int tam_parte, int n ){

        int cont = 0;
        int cont2 = 0;
        int ceros_agregados = 0; //agregado
        int tamano_leido=0;
        int tamano_original=0;//
        char c;
        char *nombreBase = concatenar(nomArchivo, ".part");
        char* nombre;
        FILE * archivoUnido;

        ceros_agregados = cargar_temporal(nomArchivo);

        tamano_original = (n*tam_parte)-ceros_agregados;
        //mientras no iteremos la misma cantidad de partes

        while((cont!=n) && (tamano_leido!=tamano_original)){

            char *num = numToChar(cont+1);
            nombre = concatenar(nombreBase, num);
            free(num);

            FILE *file=fopen(nombre,"r");
            archivoUnido=fopen(nomArchivo,"a");

            cont2=0;

            while(fread(&c, sizeof(char), 1, file) != 0 && tamano_leido!=tamano_original ){

                fwrite(&c,sizeof(char),1,archivoUnido);
                cont2++;
                tamano_leido++;
            }

            cont++;

            fclose(file);
            fclose(archivoUnido);
        }

        free(nombre);
        free(nombreBase);
}

int cargar_temporal(char *nombre){

    char *nombre_temp = concatenar(nombre, ".dato");
    FILE *temp = fopen(nombre_temp, "r");
    int dato=0;

    fread(&dato, sizeof(int), 1, temp);
    return dato;
}
