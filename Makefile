
# esto tiene su forma de trabajo
# main seria el destino, puede ser un ejecutable a crear, un archivo .o o una tarea, como clean.
# bin/main.o el requisito, el nombre del archivo que depende el destino 
# y lo de abajo, el gcc... seria el comando a ejecutar
# 
#


main:	bin/main.o bin/utils.o cortar join
		@echo "############################"
		@echo "Compilando...\n"
		gcc bin/main.o bin/utils.o -o bin/main -Wall -g
		@echo creado el ejecutable main
		@echo "\nla compilation s'est terminée avec succès" # en Frances :D

cortar:	bin/cortar.o
		@echo "############################"
		@echo "compilando cortar"
		gcc bin/cortar.o bin/utils.o -o bin/cortar -Wall -g
		
join: bin/join.o
		@echo "############################"
		@echo compilando join
		gcc bin/join.o bin/utils.o -o bin/join -Wall -g

bin/main.o: src/main.c
		@echo "############################"
		gcc -o bin/main.o src/main.c -c -g -Wall

bin/utils.o: src/utils.c src/utils.h
		@echo "############################"
		gcc -o bin/utils.o src/utils.c -c -g -Wall

bin/cortar.o: src/cortar.c src/utils.h
		@echo "############################"
		gcc -o bin/cortar.o src/cortar.c -c -g -Wall

bin/join.o: src/join.c src/utils.h
		@echo "############################"
		gcc -o bin/join.o src/join.c -c -g -Wall

clean:
	rm bin/*

